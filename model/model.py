from odoo import models, fields, api
from datetime import datetime, timedelta


class appointment(models.Model):
     _name = 'appointment.info'
     name = fields.Char(required=True)    
     service = fields.Many2one('service.list', ondelete='cascade', string="Service type", required=True)
     employee_ids = fields.Many2many('hr.employee', 'appointment_employee_rel', 'appointment_id', 'emp_id', string='practitioner',required=True)
     date_from = fields.Datetime('Start Date', index=True, copy=False, track_visibility='onchange')
     date_to = fields.Datetime('End Date', copy=False, track_visibility='onchange')
     duration = fields.Char('Duration', compute='_compute_duration', track_visibility='onchange')
     duration_temp = fields.Float('Allocation', copy=False, readonly=False)
     
     @api.multi
     @api.depends('duration_temp')
     def _compute_duration(self):
        for appointment in self:
            hours = int(appointment.duration_temp // 3600)
            minutes = int(appointment.duration_temp / 3600 % 1 * 60)
            appointment.duration = str(hours) + " : " + str(minutes)
     @api.onchange('date_from')
     def _onchange_date_from(self):
        """ If there are no date set for date_to, automatically set one 8 hours later than
            the date_from. Also update the number_of_days.
        """
        date_from = self.date_from
        date_to = self.date_to
        # No date_to set so far: automatically compute one 1 hours later
        if date_from and not date_to:
            date_to_with_delta = fields.Datetime.from_string(date_from) + timedelta(hours=1)
            self.date_to = str(date_to_with_delta)
        # Compute and update the number of hours
        if (date_to and date_from) and (date_from <= date_to):
            self.duration_temp = self._get_number_of_hours(date_from, date_to)
        else:
            self.duration_temp = 0
     def _get_number_of_hours(self, date_from, date_to):
        """ Returns a float equals to the timedelta between two dates given as string."""
        from_dt = fields.Datetime.from_string(date_from)
        to_dt = fields.Datetime.from_string(date_to)
        time_delta = to_dt - from_dt
        return time_delta.seconds
     @api.onchange('date_to')
     def _onchange_date_to(self):
        """ Update the number_of_days. """
        date_from = self.date_from
        date_to = self.date_to
        # Compute and update the number of days
        if (date_to and date_from) and (date_from <= date_to):
            self.duration_temp = self._get_number_of_hours(date_from, date_to)
        else:
            self.duration_temp = 0
class service(models.Model):
     _name = 'service.list'
     name = fields.Char()
     
